/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of FMRICommon.
*
* FMRICommon is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRICommon is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRICommon.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file mrivol.h
  \author Colin Humphries
  \brief Template classes for storing MRI data.
*/
#ifndef _MRIVOL_H
#define _MRIVOL_H

#include <cstdint>
#include <cmath>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <cassert>

// using namespace std;

namespace fmri {

  /*! \class OrientationMatrix
    \brief An orientation matrix class.
    
    This class is used to store 4x4 rotation matrices.
    
    \author Colin Humphries
  */
  class OrientationMatrix {
  public:
    /*!
      Class constructor.
    */
    OrientationMatrix() {
      Eye();
    }
    /*!
      Class constructor.
      \param dx x voxel size
      \param dy y voxel size
      \param dz z voxel size
    */
    OrientationMatrix(double dx, double dy, double dz) {
      Set(dx,dy,dz);
    }
    /*!
      Class constructor.
      \param dx x voxel size
      \param dy y voxel size
      \param dz z voxel size
      \param sx x shift
      \param sy y shift
      \param sz z shift
    */
    OrientationMatrix(double dx, double dy, double dz, 
		      double sx, double sy, double sz) {
      Set(dx,dy,dz,sx,sy,sz);
    }
    /*!
      Class constructor.
      \param dx1 orientation matrix (1,1)
      \param dx2 orientation matrix (1,2)
      \param dx3 orientation matrix (1,3)
      \param dx4 orientation matrix (1,4)
      \param dy1 orientation matrix (2,1)
      \param dy2 orientation matrix (2,2)
      \param dy3 orientation matrix (2,3)
      \param dy4 orientation matrix (2,4)
      \param dz1 orientation matrix (3,1)
      \param dz2 orientation matrix (3,2)
      \param dz3 orientation matrix (3,3)
      \param dz4 orientation matrix (3,4)
    */
    OrientationMatrix(double dx1, double dx2, double dx3, double dx4, 
		      double dy1, double dy2, double dy3, double dy4,
		      double dz1, double dz2, double dz3, double dz4) {
      Set(dx1,dx2,dx3,dx4,dy1,dy2,dy3,dy4,dz1,dz2,dz3,dz4);
    }
    /*!
      Element access operator
    */
    double& operator()(size_t m, size_t n) {
      if ((m > 2) || (n > 3)) {
	throw std::out_of_range("OrientationMatrix::() matrix index is out of range");
      }
      return orient[m][n];
    }
    /*!
      Element access operator
    */
    double const& operator()(size_t m, size_t n) const {
      if ((m > 2) || (n > 3)) {
	throw std::out_of_range("OrientationMatrix::() matrix index is out of range");
      }
      return orient[m][n];
    }
    /*!
      Copy operator
    */
    OrientationMatrix& operator=(const OrientationMatrix& origmat) {
      for (int ii=0; ii<3; ++ii) {
	for (int jj=0; jj<4; ++jj) {
	  orient[ii][jj] = origmat(ii,jj);
	}
      }
      return *this;
    }
    /*!
      Matrix multiplication operator
    */
    OrientationMatrix operator*(const OrientationMatrix &arg) const {
      OrientationMatrix smat;
      for (int ii=0; ii<3; ++ii) {
	for (int jj=0; jj<4; ++jj) {
	  smat(ii,jj) = 0.0;
	  for (int kk=0; kk<3; ++kk) {
	    smat(ii,jj) += orient[ii][kk]*arg(kk,jj);
	  }
	}
	smat(ii,3) += orient[ii][3];
      }    
      return smat;
    }
    /*!
      Set the orientation matrix to identity with defined voxel sizes.
      \param dx matrix(1,1)
      \param dy matrix(2,2)
      \param dz matrix(3,3)
    */
    void Set(double dx, double dy, double dz) {
      Eye();
      orient[0][0] = dx;
      orient[1][1] = dy;
      orient[2][2] = dz;
    }
    /*!
      Set the orientation matrix to identity with voxel sizes and shifts.
      \param dx matrix(1,1)
      \param dy matrix(2,2)
      \param dz matrix(3,3)
      \param sx matrix(1,4)
      \param sy matrix(2,4)
      \param sz matrix(3,4)
    */
    void Set(double dx, double dy, double dz, 
	     double sx, double sy, double sz) {
      Eye();
      orient[0][0] = dx;
      orient[1][1] = dy;
      orient[2][2] = dz;
      orient[0][3] = sx;
      orient[1][3] = sy;
      orient[2][3] = sz;    
    }
    /*!
      Set all values of the orientation matrix.
      \param dx1 orientation matrix (1,1)
      \param dx2 orientation matrix (1,2)
      \param dx3 orientation matrix (1,3)
      \param dx4 orientation matrix (1,4)
      \param dy1 orientation matrix (2,1)
      \param dy2 orientation matrix (2,2)
      \param dy3 orientation matrix (2,3)
      \param dy4 orientation matrix (2,4)
      \param dz1 orientation matrix (3,1)
      \param dz2 orientation matrix (3,2)
      \param dz3 orientation matrix (3,3)
      \param dz4 orientation matrix (3,4)
    */
    void Set(double dx1, double dx2, double dx3, double dx4, 
	     double dy1, double dy2, double dy3, double dy4,
	     double dz1, double dz2, double dz3, double dz4) {
      Eye();
      orient[0][0] = dx1;
      orient[0][1] = dx2;
      orient[0][2] = dx3;
      orient[0][3] = dx4;
      orient[1][0] = dy1;
      orient[1][1] = dy2;
      orient[1][2] = dy3;
      orient[1][3] = dy4;
      orient[2][0] = dz1;
      orient[2][1] = dz2;
      orient[2][2] = dz3;
      orient[2][3] = dz4;
    }
    /*!
      Set orientation matrix to the identity matrix.
    */
    void Eye() {
      orient[0][0] = 1.0;
      orient[0][1] = 0.0;
      orient[0][2] = 0.0;
      orient[0][3] = 0.0;
      orient[1][0] = 0.0;
      orient[1][1] = 1.0;
      orient[1][2] = 0.0;
      orient[1][3] = 0.0;
      orient[2][0] = 0.0;
      orient[2][1] = 0.0;
      orient[2][2] = 1.0;
      orient[2][3] = 0.0;
    }

    /*!
      Transform (rotate) point overwritting input
      \param[in,out] x x coordinate
      \param[in,out] y y coordinate
      \param[in,out] z z coordinate
    */
    void Transform(double &x, double &y, double &z) const {
      double xi, yi, zi;
      xi = x * orient[0][0] + y * orient[0][1] + z * orient[0][2] + orient[0][3];
      yi = x * orient[1][0] + y * orient[1][1] + z * orient[1][2] + orient[1][3];
      zi = x * orient[2][0] + y * orient[2][1] + z * orient[2][2] + orient[2][3];
      x = xi;
      y = yi;
      z = zi;
    }

    /*!
      Transform (roate) point
      \param[in] xi x coordinate
      \param[in] yi y coordinate
      \param[in] zi z coordinate
      \param[out] xo new x coordinate
      \param[out] yo new y coordinate
      \param[out] zo new z coordinate
    */
    void Transform(double xi, double yi, double zi, 
		   double &xo, double &yo, double &zo) const {
      xo = xi * orient[0][0] + yi * orient[0][1] + 
	zi * orient[0][2] + orient[0][3];
      yo = xi * orient[1][0] + yi * orient[1][1] + 
	zi * orient[1][2] + orient[1][3];
      zo = xi * orient[2][0] + yi * orient[2][1] + 
	zi * orient[2][2] + orient[2][3];
    }

    void Quaternion(double &qb, double &qc, double &qd, double &qf) {
      // Todo: this function will create a rotation matrix from a
      // quaternion

    }

    /*!
      Return the inverse orientation matrix.
      \return inverse orietation matrix
    */
    OrientationMatrix Inverse() const {
      OrientationMatrix minv;
      minv = *this;
      minv.Invert();
      return minv;
    }
    /*!
      Invert the current orientation matrix
    */
    void Invert() {
      double det = Determinate();
      double iorient[3][4];
      // det = 1/det;
      iorient[0][0] = (orient[1][1]*orient[2][2] - 
		       orient[1][2]*orient[2][1]) / det;
      iorient[0][1] = (orient[0][2]*orient[2][1] - 
		       orient[0][1]*orient[2][2]) / det;
      iorient[0][2] = (orient[0][1]*orient[1][2] - 
		       orient[0][2]*orient[1][1]) / det;
      iorient[1][0] = (orient[1][2]*orient[2][0] - 
		       orient[1][0]*orient[2][2]) / det;
      iorient[1][1] = (orient[0][0]*orient[2][2] - 
		       orient[0][2]*orient[2][0]) / det;
      iorient[1][2] = (orient[0][2]*orient[1][0] - 
		       orient[0][0]*orient[1][2]) / det;
      iorient[2][0] = (orient[1][0]*orient[2][1] - 
		       orient[1][1]*orient[2][0]) / det;
      iorient[2][1] = (orient[0][1]*orient[2][0] - 
		       orient[0][0]*orient[2][1]) / det;      
      iorient[2][2] = (orient[0][0]*orient[1][1] - 
		       orient[0][1]*orient[1][0]) / det;
      iorient[0][3] = -1*(orient[0][3]*iorient[0][0] +
			  orient[1][3]*iorient[0][1] +
			  orient[2][3]*iorient[0][2]);
      iorient[1][3] = -1*(orient[0][3]*iorient[1][0] + 
			  orient[1][3]*iorient[1][1] +
			  orient[2][3]*iorient[1][2]);
      iorient[2][3] = -1*(orient[0][3]*iorient[2][0] + 
			  orient[1][3]*iorient[2][1] + 
			  orient[2][3]*iorient[2][2]);
      std::copy(&iorient[0][0],&iorient[0][0]+12,&orient[0][0]);
    }
    /*!
      Calculate the determinate of the orientation matrix
      \return determinate
    */
    double Determinate() const {
      return orient[0][0]*orient[1][1]*orient[2][2] + 
	orient[1][0]*orient[2][1]*orient[0][2]
	+ orient[2][0]*orient[0][1]*orient[1][2] - 
	orient[0][0]*orient[2][1]*orient[1][2]
	- orient[2][0]*orient[1][1]*orient[0][2] - 
	orient[1][0]*orient[0][1]*orient[2][2];
    }
    /*!
      Create a 4x4 string representation of the rotation matrix
     */
    std::string String() const {
      std::ostringstream ostr;
      ostr << std::setprecision(10);
      ostr << orient[0][0] << " ";
      ostr << orient[0][1] << " ";
      ostr << orient[0][2] << " ";
      ostr << orient[0][3] << "\n";
      ostr << orient[1][0] << " ";
      ostr << orient[1][1] << " ";
      ostr << orient[1][2] << " ";
      ostr << orient[1][3] << "\n";
      ostr << orient[2][0] << " ";
      ostr << orient[2][1] << " ";
      ostr << orient[2][2] << " ";
      ostr << orient[2][3] << "\n";
      ostr << 0 << " " << 0 << " " << 0 << " " << 1 << std::endl;
      return ostr.str();
    }
    /*!
      Create a one line (1x12) string representation of the rotation matrix
     */
    std::string StringLine() const {
      std::ostringstream ostr;
      ostr << std::setprecision(10);
      ostr << orient[0][0] << " ";
      ostr << orient[0][1] << " ";
      ostr << orient[0][2] << " ";
      ostr << orient[0][3] << " ";
      ostr << orient[1][0] << " ";
      ostr << orient[1][1] << " ";
      ostr << orient[1][2] << " ";
      ostr << orient[1][3] << " ";
      ostr << orient[2][0] << " ";
      ostr << orient[2][1] << " ";
      ostr << orient[2][2] << " ";
      ostr << orient[2][3];
      return ostr.str();
    }
  private:
    double orient[3][4];
  };

  /*! \class MRIBase
    \brief Base MRI class.

    This is a minimal class with only orientation information and no data.

    \author Colin Humphries
  */
  class MRIBase {
  public:
    /*!
      Class constructor.
    */
    MRIBase() {
      SetSize(0,0,0);
    }
    /*!
      Class constructor.
      \param xs number of voxels along x
      \param ys number of voxels along y
      \param zs number of voxels along z
    */
    MRIBase(size_t xs, size_t ys, size_t zs) {
      SetSize(xs,ys,zs);
    }
    /*!
      Class constructor.
      \param xs number of voxels along x
      \param ys number of voxels along y
      \param zs number of voxels along z
      \param dx x voxel size
      \param dy y voxel size
      \param dz z voxel size
    */
    MRIBase(size_t xs, size_t ys, size_t zs, 
	    double dx, double dy, double dz) {
      SetSize(xs,ys,zs);
      SetVoxelSize(dx,dy,dz);
      orient(0,0) = dx;
      iorient(0,0) = 1/dx;
      orient(1,1) = dy;
      iorient(1,1) = 1/dy;
      orient(2,2) = dx;
      iorient(2,2) = 1/dy;
    }
    /*!
      Class constructor.
      \param xs number of voxels along x
      \param ys number of voxels along y
      \param zs number of voxels along z
      \param dx x voxel size
      \param dy y voxel size
      \param dz z voxel size
      \param om orientation matrix
    */
    MRIBase(size_t xs, size_t ys, size_t zs,
	    double dx, double dy, double dz,
	    const OrientationMatrix &om) {
      SetSize(xs,ys,zs);
      SetVoxelSize(dx,dy,dz);
      SetOrient(om);
      // Note: the voxel size is in the orientation matrix
      // so this function should probably include code to
      // verify that they correspond.
    }
    MRIBase(const MRIBase &ob) {
      SetSize(ob.XSize(),ob.YSize(),ob.ZSize());
      SetVoxelSize(ob.XVoxelSize(),ob.YVoxelSize(),ob.ZVoxelSize());
      SetOrient(ob.OrientMatrix());
    }
    /*!
      Number of dimensions.
      \return number of dimensions in the array
    */
    virtual size_t NumDims() const {return 3;}
    /*!
      Number of voxels along x.
      \return number of x voxels
    */
    size_t XSize() const {return isize[0];}
    /*!
      Number of voxels along y.
      \return number of y voxels
    */
    size_t YSize() const {return isize[1];}
    /*!
      Number of voxels along z.
      \return number of z voxels
    */
    size_t ZSize() const {return isize[2];}
    virtual size_t TSize() const {return 1;}
    /*!
      Number of voxels along specified dimesion.
      \param dimval one of the dimensions of the MRI array
      \return number of voxels
    */
    size_t Size(size_t dimval) const {
      if (dimval > 2) {
	throw std::invalid_argument("MRIBase::Size() dimension greater than 2");
      }
      return isize[dimval];
    }
    void SetSize(size_t xs, size_t ys, size_t zs) {
      isize[0] = xs;
      isize[1] = ys;
      isize[2] = zs;
    }
    void SetSize(const MRIBase &vol) {
      isize[0] = vol.XSize();
      isize[1] = vol.YSize();
      isize[2] = vol.ZSize();
    }
    virtual size_t NumElements() const {return isize[0]*isize[1]*isize[2];}

    double XVoxelSize() const {return vsize[0];}
    double YVoxelSize() const {return vsize[1];}
    double ZVoxelSize() const {return vsize[2];}
    virtual double TVoxelSize() const {return 0.0;}
    double VoxelSize(size_t ii) const {return vsize[ii];}
    void SetVoxelSize(double dx, double dy, double dz)  {
      vsize[0] = dx;
      vsize[1] = dy;
      vsize[2] = dz;
    }
    void SetVoxelSize(const MRIBase &vol)  {
      vsize[0] = vol.XVoxelSize();
      vsize[1] = vol.YVoxelSize();
      vsize[2] = vol.ZVoxelSize();
    }

    double Orient(size_t m, size_t n) const {return orient(m,n);}
    double InvOrient(size_t m, size_t n) const {return iorient(m,n);}
    OrientationMatrix& OrientMatrix() {return orient;}
    OrientationMatrix const& OrientMatrix() const {return orient;}
    void SetOrient(const OrientationMatrix &om) {
      orient = om;
      iorient = om.Inverse();
    }
    void SetOrient(const MRIBase &vol) {
      orient = vol.OrientMatrix();
      iorient = orient.Inverse();
    }

    void SetGeometry(const MRIBase &vol) {
      SetSize(vol);
      SetVoxelSize(vol);
      SetOrient(vol);
    }
    /*!
      Convert a position (mm) in the volume to x,y,z voxel indices
     */
    bool Pos2Vox(double x, double y, double z, 
		 size_t &xi, size_t &yi, size_t &zi) const {
      bool involume = true;
      iorient.Transform(x,y,z);
      // xi = (int)floor(x);
      // yi = (int)floor(y);
      // zi = (int)floor(z);
      // Note: according to the NIFTI header the point x,y,z refers to the center of the voxel
      xi = (int)round(x);
      yi = (int)round(y);
      zi = (int)round(z);
      if (xi < 0) {
	xi = 0;
	involume = false;
      }
      if (xi > XSize()) {
	xi = XSize();
	involume = false;
      }
      if (yi < 0) {
	yi = 0;
	involume = false;
      }
      if (yi > YSize()) {
	yi = YSize();
	involume = false;
      }
      if (zi < 0) {
	zi = 0;
	involume = false;
      }
      if (zi > ZSize()) {
	zi = ZSize();
	involume = false;
      }
      return involume;
    }
    /*!
      Convert x,y,z voxel indices to a position (mm) in the volume
     */
    void Vox2Pos(size_t xi, size_t yi, size_t zi,
		 double &x, double &y, double &z) const {
      x = (double)xi;
      y = (double)yi;
      z = (double)zi;
      orient.Transform(x,y,z);
    }

    virtual void Vox2Pos(size_t vi,
			 double &x, double &y, double &z) const {
      // std::cout << "test" << endl;
      x = std::fmod((double)vi,(double)isize[0]);
      y = std::fmod((double)(vi/isize[0]),(double)isize[1]);
      z = (double)(vi/(isize[0]*isize[1]));
      // cout << x << " " << y << " " << z << endl;
      orient.Transform(x,y,z);
    }
  private:
    OrientationMatrix orient;
    OrientationMatrix iorient;
    size_t isize[3];
    double vsize[3];
  };

  /*! \class MRIVol
    \brief Single volume MRI class.

    This is a class for storing the data from a single 3d MRI volume.

    \author Colin Humphries
  */
  template <class T>
  class MRIVol : public MRIBase {
  public:
    /*!
      Class constructor.
    */
    MRIVol() {
      pdata = nullptr;
    }
    /*!
      Class constructor.
    */
    MRIVol(size_t xs, size_t ys, size_t zs) : MRIBase(xs,ys,zs) {
      pdata = nullptr;
      allocatedata();
    }
    /*!
      Class constructor.
    */
    MRIVol(size_t xs, size_t ys, size_t zs, 
	   double dx, double dy, double dz) : MRIBase(xs,ys,zs,dx,dy,dz) {
      pdata = nullptr;
      allocatedata();
    }
    /*!
      Class constructor.
    */
    MRIVol(size_t xs, size_t ys, size_t zs,
	   double dx, double dy, double dz,
	   const OrientationMatrix &om) : MRIBase(xs,ys,zs,dx,dy,dz,om) {
      pdata = nullptr;
      allocatedata();
    }
    /*!
      Class destructor.
    */
    ~MRIVol() {
      cleardata();
    }
    void Resize(size_t xs, size_t ys, size_t zs) {
      SetSize(xs,ys,zs);
      allocatedata();
    }
    /*!
      Set all voxels to a single value.
      \param val new voxel value
    */
    void Fill(T val) {
      for (int ii=0; ii<NumElements(); ++ii) {
	pdata[ii] = val;
      }
    }
    /*!
      Element access operator
    */
    T& operator()(size_t x, size_t y, size_t z) {
      if ((x >= XSize()) || (y >= YSize()) || (z >= ZSize())) {
	throw std::out_of_range("MRIVol::() volume index is out of range");
      }
      return pdata[x+y*XSize()+z*XSize()*YSize()];
    }
    /*!
      Element access operator
    */
    T const& operator()(size_t x, size_t y, size_t z) const {
      if ((x >= XSize()) || (y >= YSize()) || (z >= ZSize())) {
	throw std::out_of_range("MRIVol::() volume index is out of range");
      }    
      return pdata[x+y*XSize()+z*XSize()*YSize()];
    }
    /*!
      Element access operator
      \param ind voxel index
    */
    T& operator[](size_t ind) {
      if (ind >= NumElements()) {
	throw std::out_of_range("MRIVol::[] index is out of range");
      }
      return pdata[ind];
    }
    /*!
      Element access operator
      \param ind voxel index
    */
    T const& operator[](size_t ind) const {
      if (ind >= NumElements()) {
	throw std::out_of_range("MRIVol::[] index is out of range");
      }
      return pdata[ind];
    }
    /*!
      Copy operator
    */
    MRIVol<T> & operator= (const MRIVol<T> &avol) {
      SetGeometry(avol);
      allocatedata();
      for (int ii=0; ii<NumElements(); ++ii) {
	pdata[ii] = avol[ii];
      }
      return *this;
    }
    T* data_ptr() {return pdata;}

  private:
    void allocatedata() {
      if (pdata) {
	cleardata();
      }
      if (XSize() && YSize() && ZSize()) {
	pdata = new T[XSize()*YSize()*ZSize()];
      }
    }
    void cleardata() {
      if (pdata) {
	delete [] pdata;
	pdata = nullptr;
      }
    }
    T *pdata;
  };

  /*! \class MRIData
    \brief Multiple volume MRI class.

    This is a class for storing multiple 3d MRI volumes.

    The data are stored in a 2-dimensional array (matrix) to simplify
    computation. The class also supports only storing a subset of the
    voxels in the volume.

    \author Colin Humphries
  */
  template <class T>
  class MRIData : public MRIBase {
  public:
    MRIData() {
      pdata = nullptr;
      tsize = 0;
      tlength = 0;
      numvox = 0;
      indexed = false;
      pindex1 = nullptr;
      pindex2 = nullptr;
    }
    MRIData(size_t xs, size_t ys, size_t zs, size_t ts) : MRIBase(xs,ys,zs) {
      pdata = nullptr;
      tsize = ts;
      tlength = 0;
      indexed = false;
      numvox = xs*ys*zs;
      allocatedata();
      pindex1 = nullptr;
      pindex2 = nullptr;
    }
    MRIData(size_t xs, size_t ys, size_t zs, size_t ts, 
	    double dx, double dy, double dz) : MRIBase(xs,ys,zs,dx,dy,dz) {
      pdata = nullptr;
      tsize = ts;
      tlength = 0;
      indexed = false;
      numvox = xs*ys*zs;
      allocatedata();
      pindex1 = nullptr;
      pindex2 = nullptr;
    }
    MRIData(size_t xs, size_t ys, size_t zs, size_t ts,
	    double dx, double dy, double dz,
	    const OrientationMatrix &om) : MRIBase(xs,ys,zs,dx,dy,dz,om) {
      pdata = nullptr;
      tsize = ts;
      tlength = 0;
      indexed = false;
      numvox = xs*ys*zs;
      allocatedata();
      pindex1 = nullptr;
      pindex2 = nullptr;
    }
    MRIData(const MRIData &dvol) : MRIBase(dvol) {
      pdata = nullptr;
      indexed = false;
      pindex1 = nullptr;
      pindex2 = nullptr;
      Resize(dvol);
    }
    MRIData(const MRIData &dvol, size_t ts) : MRIBase(dvol) {
      pdata = nullptr;
      indexed = false;
      pindex1 = nullptr;
      pindex2 = nullptr;
      Resize(dvol,ts);
    }  
    ~MRIData() {
      cleardata();
    }

    size_t NumDims() const {
      if (tsize > 1) {
	return 4;
      }
      else {
	return 3;
      }
    };
    size_t TSize() const {return tsize;}
    double TVoxelSize() const {return tlength;}
    void SetTVoxelSize(double tl) {tlength = tl;}
  
    void Resize(size_t xs, size_t ys, size_t zs, size_t ts) {
      SetSize(xs,ys,zs);
      numvox = xs*ys*zs;
      tsize = ts;
      tlength = 1.0;
      allocatedata();
    }
    void Resize(size_t xs, size_t ys, size_t zs, size_t ts, double tl) {
      SetSize(xs,ys,zs);
      numvox = xs*ys*zs;
      tsize = ts;
      tlength = tl;
      allocatedata();
    }
    template <class T2>
    void Resize(const MRIVol<T2> &target, size_t ts) {
      SetGeometry(target);
      numvox = NumElements();
      tsize = ts;
      tlength = 1.0;
      allocatedata();
    }
    template <class T2>
    void Resize(const MRIVol<T2> &target, size_t ts, double tl) {
      SetGeometry(target);
      numvox = NumElements();
      tsize = ts;
      tlength = tl;
      allocatedata();
    }
    template <class T2>
    void Resize(const MRIData<T2> &target) {
      SetGeometry(target);
      numvox = target.NumVoxels();
      tsize = target.NumTimePnts();
      tlength = target.TVoxelSize();
      allocatedata();
      if (target.IsIndexed()) {
	if (pindex1) {
	  delete [] pindex1;
	}
	if (pindex2) {
	  delete [] pindex2;
	}
	pindex1 = new int32_t[target.NumTotalVoxels()];
	pindex2 = new int32_t[numvox];
	target.CopyIndex(pindex1,pindex2);
	indexed = true;
      }
    }  
    template <class T2>
    void Resize(const MRIData<T2> &target, size_t ts) {
      SetGeometry(target);
      numvox = target.NumVoxels();
      tsize = ts;
      tlength = target.TVoxelSize();
      allocatedata();
      if (target.IsIndexed()) {
	if (pindex1) {
	  delete [] pindex1;
	}
	if (pindex2) {
	  delete [] pindex2;
	}
	pindex1 = new int32_t[target.NumTotalVoxels()];
	pindex2 = new int32_t[numvox];
	target.CopyIndex(pindex1,pindex2);
	indexed = true;
      }
    }
  
    template <class T2>
    void ResizeIndexed(const MRIVol<T2> &mask, size_t ts) {
      // SetSize(mask);
      SetGeometry(mask);
      tlength = 1.0;
      int numel = 0;
      for (size_t ii=0; ii<mask.NumElements(); ++ii) {
	if (mask[ii]) {
	  ++numel;
	}
      }
      numvox = numel;
      tsize = ts;
      allocatedata();
      if (pindex1) {
	delete [] pindex1;
      }
      if (pindex2) {
	delete [] pindex2;
      }
      pindex1 = new int32_t[NumTotalVoxels()];
      pindex2 = new int32_t[numvox];
      numel = 0;
      for (size_t ii=0; ii<mask.NumElements(); ++ii) {
	if (mask[ii]) {
	  pindex1[ii] = numel;
	  pindex2[numel] = ii;
	  ++numel;
	}
      }
      indexed = true;
    }
    template <class T2>
    void ResizeIndexed(const MRIVol<T2> &mask, size_t ts, double tl) {
      ResizeIndexed(mask,ts);
      tlength = tl;
    }
    void Fill(T val) {
      for (int ii=0; ii<NumElements(); ++ii) {
	pdata[ii] = val;
      }
    }
    T& operator()(size_t m, size_t n) {
      return pdata[m+n*tsize];
    }
    T const& operator()(size_t m, size_t n) const {
      return pdata[m+n*tsize];
    }
    T& operator[](size_t ind) {
      return pdata[ind];
    }
    T const& operator[](size_t ind) const {
      return pdata[ind];
    }
    void ReadSubVolume(MRIVol<T> &vol, size_t t) const {
      // note: add check and resize
      if (indexed) {
	for (size_t ii = 0; ii<numvox; ++ii) {
	  vol[pindex2[ii]] = pdata[t+ii*tsize];
	}
      }
      else {
	for (size_t ii = 0; ii<numvox; ++ii) {
	  vol[ii] = pdata[t+ii*tsize];
	}
      }
    }
    void SetSubVolume(size_t ind, const MRIData<T> &vol, size_t volind) {
      assert(ind < tsize);
      assert(numvox == vol.NumVoxels());
      for (size_t ii = 0; ii<numvox; ++ii) {
	pdata[ind+ii*tsize] = vol(volind,ii);
      }
    }
    void CopyIndex(int32_t *pi_1, int32_t *pi_2) const {
      // pindex1 = new int32_t[NumTotalVoxels()];
      // pindex2 = new int32_t[numvox];
      for (size_t ii=0; ii<NumTotalVoxels(); ++ii) {
	pi_1[ii] = pindex1[ii];
      }
      for (size_t ii=0; ii<numvox; ++ii) {
	pi_2[ii] = pindex2[ii];
      }
    }
    bool Element(size_t x, size_t y, size_t z, size_t t, T &val) const {
      if (indexed) {
	val = pdata[t+tsize*(pindex1[x+y*XSize()+z*XSize()*YSize()])];
      }
      else {
	val = pdata[t+tsize*(x+y*XSize()+z*XSize()*YSize())];
      }
      return true;
    }
    bool SetElement(size_t x, size_t y, size_t z, size_t t, const T &val) {
      if (indexed) {
	pdata[t+tsize*(pindex1[x+y*XSize()+z*XSize()*YSize()])] = val;
      }
      else {
	pdata[t+x*tsize+y*tsize*XSize()+z*tsize*XSize()*YSize()] = val;
      }
      return true;
    }
    size_t NumElements() const {return numvox*tsize;}
    size_t NumVoxels() const {return numvox;}
    size_t Cols() const {return numvox;}
    size_t NumTotalVoxels() const {return XSize()*YSize()*ZSize();}
    size_t NumTimePnts() const {return tsize;}
    size_t Rows() const {return tsize;}
    T* data_ptr() {return pdata;}

    bool IsIndexed() const {return indexed;}

    void Vox2Pos(size_t vi,
		 double &x, double &y, double &z) const {
      if (indexed) {
	vi = pindex2[vi];
      }
      MRIBase::Vox2Pos(vi,x,y,z);
      // x = std::fmod((double)vi,(double)XSize());
      // y = std::fmod((double)(vi/XSize()),(double)YSize());
      // z = (double)(vi/(XSize()*YSize()));
      // orient.Transform(x,y,z);
    }

    size_t Vox2Vox(size_t vi) const {
      if (indexed) {
	return pindex2[vi];
      }
      return vi;
    }
    size_t Index2Vox(size_t vi) const {
      if (indexed) {
	return pindex2[vi];
      }
      return vi;
    }
    size_t Vox2Index(size_t vv) const {
      if (indexed) {
	return pindex1[vv];
      }
      return vv;
    }

  private:
    void allocatedata() {
      if (pdata) {
	cleardata();
      }
      if (numvox && tsize) {
	pdata = new T[numvox*tsize];
      }
    }
    void cleardata() {
      if (pdata) {
	delete [] pdata;
	pdata = nullptr;
      }
      if (pindex1) {
	delete [] pindex1;
	pindex1 = nullptr;
      }
      if (pindex2) {
	delete [] pindex2;
	pindex2 = nullptr;
      }
    }
    T *pdata;
    size_t tsize;
    double tlength;
    size_t numvox;
    int32_t *pindex1;
    int32_t *pindex2;
    bool indexed;
  };

}

#endif
