/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of FMRICommon.
*
* FMRICommon is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRICommon is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRICommon.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file niftifile.h
  \author Colin Humphries
  \brief Classes for reading and writing NIFTI files.
*/
#ifndef _NIFTIFILE_H
#define _NIFTIFILE_H

#include <string>
#include <cstring>
#include <fstream>
#include "nifti1.h"
#include "mrivol.h"

#ifndef NO_ZLIB
#include "zlib.h"
#endif



/*
Notes: When a file is opened ie Open() the entire NIFTI header is
stored. This header is preserved after Close(). If this instance is
used to write out a new volume then some of the header information
including the slice info, units, and intent codes will be written to
the new file. To clear this information use the ClearHeader()
function.

 */

// using namespace std;

namespace fmri {

  enum class FileMode {IN, OUT};

  /*! \class FileHandleBase
    \brief Generic file i/o class
  */
  class FileHandleBase {
  public:
    /*!
      Class constructor.
    */
    FileHandleBase() {
      failflag = false;
      eofflag = false;
      isopen = false;
    }
    virtual void Open(const std::string &, FileMode) = 0;
    virtual void Close() = 0;
    virtual void Read(char *, int) = 0;
    virtual void Write(char *, int) = 0;
    virtual void Seek(int) = 0;
    bool Fail() {return failflag;}
    bool End() {return eofflag;}
    bool IsOpen() {return isopen;}
  protected:
    bool failflag;
    bool eofflag;
    bool isopen;
  };

  /*! \class FileHandle
    \brief File i/o using standard fstream
  */
  class FileHandle : public FileHandleBase {
  public:
    /*!
      Class constructor.
    */  
    FileHandle() {;}
    /*!
      Open file for reading/writing
      \param[in] filename Name of file to open
      \param[in] fm file mode
    */
    void Open(const std::string &filename, FileMode fm) {
      if (fm == FileMode::IN) {
	fio.open(filename,std::fstream::in | std::fstream::binary);
      }
      else {
	fio.open(filename,std::fstream::out | std::fstream::binary);
      }
      failflag = fio.fail();
      if (!failflag) {
	isopen = true;
      }
    }
    /*!
      Close file
    */  
    void Close() {
      if (isopen) {
	fio.close();
	failflag = fio.fail();
	isopen = false;
      }
      else {
	failflag = false;
      }
    }
    /*!
      Read binary data
      \param[in] buffer input buffer
      \param[in] numb number of bytes
    */  
    void Read(char *buffer, int numb) {
      fio.read(buffer,numb);
      failflag = fio.fail();
      eofflag = fio.eof();
    }
    /*!
      Write binary data
      \param[in] buffer output buffer
      \param[in] numb number of bytes
    */  
    void Write(char *buffer, int numb) {
      fio.write(buffer,numb);
      failflag = fio.fail();
    }
    /*!
      Set file access position
      \param[in] numb byte offset
    */  
    void Seek(int numb) {
      fio.seekp(numb);
    }
  private:
    /*!
      File handle
    */  
    std::fstream fio;
  };

  /*! \class FileHandleGz
    \brief File i/o using gzip interface
  */
  class FileHandleGZ : public FileHandleBase {
  public:
    /*!
      Class constructor.
    */  
    FileHandleGZ() {;}
    /*!
      Open file for reading/writing
      \param[in] filename Name of file to open
      \param[in] fm file mode
    */  
    void Open(const std::string &filename, FileMode fm) {
      if (fm == FileMode::IN) {
	fio = gzopen(filename.c_str(),"rb");      
      }
      else {
	fio = gzopen(filename.c_str(),"wb");
      }
      if (!fio) {
	failflag = true;
      }
      else {
	isopen = true;
      }
    }
    /*!
      Close file
    */  
    void Close() {
      if (fio) {
	gzclose(fio);
	fio = Z_NULL;
      }
      else {
	failflag = true;
      }
      isopen = false;
    }
    /*!
      Read binary data
      \param[in] buffer input buffer
      \param[in] numb number of bytes
    */  
    void Read(char *buffer, int numb) {
      if (gzread(fio,buffer,numb) <= 0) {
	failflag = true;
      }
    }
    /*!
      Write binary data
      \param[in] buffer output buffer
      \param[in] numb number of bytes
    */  
    void Write(char *buffer, int numb) {
      if (gzwrite(fio,buffer,numb) <= 0) {
	failflag = true;
      }
    }
    /*!
      Set file access position
      \param[in] numb byte offset
    */  
    void Seek(int numb) {
      gzseek(fio,numb,SEEK_SET);    
    }
  private:
    /*!
      File handle
    */  
    gzFile fio;
  };

  enum class NIFTIOutputMode {DEFAULT, NOZIP, GZIP, BZIP};

  /*!
    \class NIFTIFile
    \brief A NIFTI I/O class

    This class is used to read and write MRI data to NIFTI files.

    \author Colin Humphries
  */
  class NIFTIFile {
  public:
    /*!
      Class constructor.
    */
    NIFTIFile() {
      p_nheader1 = new nifti_1_header;
      pfio = nullptr;
      // fgzio = Z_NULL;
      nmode = NIFTIOutputMode::DEFAULT;
      ClearHeader();
      calc_orient = true;
    }
    /*!
      Class destructor.
    */  
    ~NIFTIFile() {
      if (p_nheader1) {
	delete p_nheader1;
      }
      Close();
    }
  
    bool Open(const std::string &filename, char perm) {
      if (perm == 'w') {
	NIFTIOutputMode nm = nmode;
	if (nm == NIFTIOutputMode::DEFAULT) {
	  GetOutputMode(filename,nm);
	}
	if (nm == NIFTIOutputMode::GZIP) {
	  pfio = (FileHandleBase *)new FileHandleGZ();
	}
	else {
	  pfio = (FileHandleBase *)new FileHandle();
	}
	pfio->Open(filename,FileMode::OUT);
	if (pfio->Fail()) {
	  return false;
	}
	ClearHeader();
      }
      else if (perm == 'r') {
	pfio = (FileHandleBase *)new FileHandleGZ();
	pfio->Open(filename,FileMode::IN);
	if (pfio->Fail()) {
	  return false;
	}
	pfio->Read((char *)p_nheader1,sizeof(nifti_1_header));
	if (pfio->Fail()) {
	  return false;
	}
      }
      else {
	return false;
      }
      return true;
    }

    bool Close() {
      if (!pfio) {
	return false;
      }
      pfio->Close();
      return true;
    }

    bool Read(MRIBase &vol) {
      // Load only header information into a MRIBase object
      if (!pfio) {
	return false;
      }
      if (NumDims() < 3) {
	errorstring = "Less than three dimensions in niftifile";
	return false;
      }
      if (p_nheader1->dim[1] < 1 || p_nheader1->dim[2] < 1 || 
	  p_nheader1->dim[3] < 1) {
	errorstring = "Invalid dimension values in nifti header";
	return false;
      }
      SetHeaderInfo(vol);
      vol.SetSize(p_nheader1->dim[1],p_nheader1->dim[2],p_nheader1->dim[3]);
      return true;
    }

    template <class T>
    bool Read(MRIVol<T> &vol) {
      if (!pfio) {
	return false;
      }
      if (NumDims() < 3) {
	errorstring = "Less than three dimensions in niftifile";
	return false;
      }
      if (p_nheader1->dim[1] < 1 || p_nheader1->dim[2] < 1 || 
	  p_nheader1->dim[3] < 1) {
	errorstring = "Invalid dimension values in nifti header";
	return false;
      }
      SetHeaderInfo(vol);
      vol.Resize(p_nheader1->dim[1],p_nheader1->dim[2],p_nheader1->dim[3]);
      pfio->Seek((int)p_nheader1->vox_offset);
      size_t numel = vol.NumElements();

      char *buffer = new char[numel * (p_nheader1->bitpix / 8)];
      pfio->Read(buffer,numel*(p_nheader1->bitpix/8));
      MoveDataIn(vol.data_ptr(),buffer,numel,p_nheader1->datatype);
      delete [] buffer;
      return true;
    }
  
    template <class T>
    bool Read(MRIData<T> &vol) {
      if (!pfio) {
	return false;
      }
      if (NumDims() < 3) {
	errorstring = "Less than three dimensions in niftifile";
	return false;
      }
      int numtp = 1;
      // Note: We only care about 4-d data. We ignore additional data
      // and only take the first 4-d volume. According to the nifti
      // docs, if the data is a timecourse then the dimension should be
      // listed in the 4th dimension entry, if it it multiple volumes
      // (eg. statistical volumes) then this should be listed in the 5th
      // dimension. Many programs ignore this and only use the 4th
      // dimension. The following gets the number of volumes from either
      // the 4th or 5th.
      if (NumDims() >= 4) {
	if (p_nheader1->dim[4] == 1) {
	  if (NumDims() > 5) {
	    numtp = p_nheader1->dim[5];
	  }
	}
	else {
	  numtp = p_nheader1->dim[4];
	}
      }
      if (numtp < 1 || p_nheader1->dim[1] < 1 || p_nheader1->dim[2] < 1 || 
	  p_nheader1->dim[3] < 1) {
	errorstring = "Invalid dimension values in nifti header";
	return false;
      }
      SetHeaderInfo(vol);
      vol.Resize(p_nheader1->dim[1],p_nheader1->dim[2],p_nheader1->dim[3],numtp);
      pfio->Seek((int)p_nheader1->vox_offset);
      size_t numv = Size(0)*Size(1)*Size(2);
      size_t numel = vol.NumElements();
      char *buffer = new char[numel * (p_nheader1->bitpix / 8)];
      pfio->Read(buffer,numel*(p_nheader1->bitpix/8));
      T *fbuffer = new T[numv];
      for (int ii=0; ii<numtp; ++ii) {
	MoveDataIn(fbuffer,buffer+(ii*numv*(p_nheader1->bitpix/8)),
		   numv,p_nheader1->datatype);
	for (size_t jj=0; jj<numv; ++jj) {
	  vol(ii,jj) = fbuffer[jj];
	}
      }
      delete [] fbuffer;
      delete [] buffer;
      return true;
    }

    template <class T1, class T2>
    bool Read(MRIData<T1> &vol, const MRIVol<T2> &mask) {
      if (!pfio) {
	return false;
      }
      if (NumDims() < 3) {
	errorstring = "Less than three dimensions in niftifile";
	return false;
      }
      // cout << mask.XSize() << endl;
      // cout << p_nheader1->dim[1] << endl;
      if (p_nheader1->dim[1] != (int)mask.XSize() 
	  || p_nheader1->dim[2] != (int)mask.YSize() 
	  || p_nheader1->dim[3] != (int)mask.ZSize()) {
	errorstring = "Mask and opened file have different dimensions";
	return false;
      }
      int numtp = 1;
      if (NumDims() >= 4) {
	if (p_nheader1->dim[4] == 1) {
	  if (NumDims() > 5) {
	    numtp = p_nheader1->dim[5];
	  }
	}
	else {
	  numtp = p_nheader1->dim[4];
	}
      }
      SetHeaderInfo(vol);
      vol.ResizeIndexed(mask,numtp);
      size_t numv = Size(0)*Size(1)*Size(2);
      size_t numel = numv*numtp;
      pfio->Seek((int)p_nheader1->vox_offset);
      char *buffer = new char[numel * (p_nheader1->bitpix / 8)];
      pfio->Read(buffer,numel*(p_nheader1->bitpix/8));
      T1 *fbuffer = new T1[numv];
      for (int ii=0; ii<numtp; ++ii) {
	MoveDataIn(fbuffer,buffer+(ii*numv*(p_nheader1->bitpix/8)),
		   numv,p_nheader1->datatype);
	size_t count = 0;
	for (size_t jj=0; jj<mask.NumElements(); ++jj) {
	  if (mask[jj]) {
	    vol(ii,count) = fbuffer[jj];
	    ++count;
	  }
	}
      }
      delete [] fbuffer;
      delete [] buffer;
      return true;
    }

    template <class T>
    bool Write(MRIVol<T> &vol, int datatype) {
      if (!pfio) {
	return false;
      }
      GetHeaderInfo(vol);
      p_nheader1->datatype = datatype;
      SetBitPix();
      size_t numel = vol.NumElements();
      int32_t extension = 0;
      char *buffer = new char[numel * (p_nheader1->bitpix / 8)];
      MoveDataOut(vol.data_ptr(),buffer,numel,p_nheader1->datatype);

      pfio->Write((char *)p_nheader1,sizeof(nifti_1_header));
      pfio->Write((char *)&extension,4*sizeof(char));
      pfio->Write(buffer,numel*(p_nheader1->bitpix / 8));

      delete [] buffer;
      return true;
    }

    template <class T>
    bool Write(MRIData<T> &vol, int datatype) {
      if (!pfio) {
	return false;
      }
      GetHeaderInfo(vol);
      p_nheader1->datatype = datatype;
      SetBitPix();
      int32_t extension = 0;
      size_t numtp = vol.NumTimePnts();
      size_t numtv = vol.NumTotalVoxels();
      char *buffer = new char[numtv*(p_nheader1->bitpix / 8)];
      MRIVol<T> tvol(vol.XSize(),vol.YSize(),vol.ZSize());
    
      pfio->Write((char *)p_nheader1,sizeof(nifti_1_header));
      pfio->Write((char *)&extension,4*sizeof(char));
      for (size_t ii=0; ii<numtp; ++ii) {
	vol.ReadSubVolume(tvol,ii);
	MoveDataOut(tvol.data_ptr(),buffer,numtv,p_nheader1->datatype);
	pfio->Write(buffer,numtv*(p_nheader1->bitpix / 8));
      }
      delete [] buffer;
      return true;
    }

    template <class T>
    bool Write(MRIData<T> &vol, const std::vector<int> &subindex, int datatype) {
      if (!pfio) {
	return false;
      }
      if (subindex.size() == 0) {
	return false;
      }
      size_t numtp = vol.NumTimePnts();
      size_t numtv = vol.NumTotalVoxels();
      for (std::vector<int>::const_iterator it = subindex.begin(); it != subindex.end(); ++it) {
	if ((*it < 0) || (*it >= (int)numtp)) {
	  return false;
	}
      }
      GetHeaderInfo(vol);
      p_nheader1->dim[4] = subindex.size(); // Set the time dimension to a different size
      p_nheader1->datatype = datatype;
      SetBitPix();
      int32_t extension = 0;
    
      char *buffer = new char[numtv*(p_nheader1->bitpix / 8)];
      MRIVol<T> tvol(vol.XSize(),vol.YSize(),vol.ZSize());
    
      pfio->Write((char *)p_nheader1,sizeof(nifti_1_header));
      pfio->Write((char *)&extension,4*sizeof(char));
      // for (size_t ii=0; ii<numtp; ++ii) {
      for (std::vector<int>::const_iterator it = subindex.begin(); it != subindex.end(); ++it) {
	vol.ReadSubVolume(tvol,*it);
	MoveDataOut(tvol.data_ptr(),buffer,numtv,p_nheader1->datatype);
	pfio->Write(buffer,numtv*(p_nheader1->bitpix / 8));
      }
      delete [] buffer;
      return true;
    }

    int Datatype() const {
      return p_nheader1->datatype;
    }

    int Size(size_t ind) const {
      return p_nheader1->dim[ind+1];
    }
    int XSize() const {
      return p_nheader1->dim[1];
    }
    int YSize() const {
      return p_nheader1->dim[2];
    }
    int ZSize() const {
      return p_nheader1->dim[3];
    }

    float VoxelSize(size_t ind) const {
      return p_nheader1->pixdim[ind+1];
    }

    // float Orient(int,int) const;
    int NumDims() const {
      return p_nheader1->dim[0];
    }

    nifti_1_header* Header() {return p_nheader1;};

    void CopyGeometry(MRIBase &vol) {
      vol.SetSize(XSize(),YSize(),ZSize());
      vol.SetVoxelSize(p_nheader1->pixdim[1],p_nheader1->pixdim[2],
		       p_nheader1->pixdim[3]);
      OrientationMatrix orient;
      orient(0,0) = p_nheader1->srow_x[0];
      orient(0,1) = p_nheader1->srow_x[1];
      orient(0,2) = p_nheader1->srow_x[2];
      orient(0,3) = p_nheader1->srow_x[3];
      orient(1,0) = p_nheader1->srow_y[0];
      orient(1,1) = p_nheader1->srow_y[1];
      orient(1,2) = p_nheader1->srow_y[2];
      orient(1,3) = p_nheader1->srow_y[3];
      orient(2,0) = p_nheader1->srow_z[0];
      orient(2,1) = p_nheader1->srow_z[1];
      orient(2,2) = p_nheader1->srow_z[2];
      orient(2,3) = p_nheader1->srow_z[3];
      vol.SetOrient(orient);
    
    }

    void ClearHeader() {
      p_nheader1->sizeof_hdr = 348;
      p_nheader1->vox_offset = 352;
      p_nheader1->scl_slope = 1;
      p_nheader1->scl_inter = 0;
      p_nheader1->cal_min = 0;
      p_nheader1->cal_max = 0;
      std::strcpy(p_nheader1->magic,"n+1");
      for (int ii=0; ii<8; ++ii) {
	p_nheader1->pixdim[ii] = 0;
	p_nheader1->dim[ii] = 1;
      }
      p_nheader1->xyzt_units = 0;
      p_nheader1->slice_code = 0;
      p_nheader1->dim_info = 0;
      p_nheader1->slice_start = 0;
      p_nheader1->slice_end = 0;
      std::strcpy(p_nheader1->descrip,"niftifile.h");
      p_nheader1->intent_code = NIFTI_INTENT_NONE;
      p_nheader1->intent_p1 = 0;
      p_nheader1->intent_p2 = 0;
      p_nheader1->intent_p3 = 0;
      p_nheader1->intent_name[0] = 0;
      p_nheader1->aux_file[0] = 0;
      p_nheader1->toffset = 0;
      p_nheader1->data_type[0] = 0;
      p_nheader1->db_name[0] = 0;
      p_nheader1->slice_duration = 0;
      p_nheader1->toffset = 0;
      p_nheader1->dim_info = 0;
      p_nheader1->extents = 0;
      p_nheader1->session_error = 0;
      p_nheader1->regular = 0;
      p_nheader1->glmin = 0;
      p_nheader1->glmax = 0;
      p_nheader1->qform_code = 0;
      p_nheader1->sform_code = 0;
      calc_orient = true;
    }

    void CopyHeaderOrient(NIFTIFile &fin) {
      calc_orient = false;
      nifti_1_header *ph;
      ph = fin.Header();
      p_nheader1->qform_code = ph->qform_code;
      p_nheader1->quatern_b = ph->quatern_b;
      p_nheader1->quatern_c = ph->quatern_c;
      p_nheader1->quatern_d = ph->quatern_d;
      p_nheader1->qoffset_x = ph->qoffset_x;
      p_nheader1->qoffset_y = ph->qoffset_y;
      p_nheader1->qoffset_z = ph->qoffset_z;
      p_nheader1->pixdim[0] = ph->pixdim[0];
      p_nheader1->pixdim[1] = ph->pixdim[1];
      p_nheader1->pixdim[2] = ph->pixdim[2];
      p_nheader1->pixdim[3] = ph->pixdim[3];

      p_nheader1->sform_code = ph->sform_code;
      for (uint ii=0; ii<4; ++ii) {
	p_nheader1->srow_x[ii] = ph->srow_x[ii];
	p_nheader1->srow_y[ii] = ph->srow_y[ii];
	p_nheader1->srow_z[ii] = ph->srow_z[ii];
      }
    }
    void CopyHeaderIntent(NIFTIFile &fin) {
      nifti_1_header *ph;
      ph = fin.Header();
      p_nheader1->intent_code = ph->intent_code;
      p_nheader1->intent_p1 = ph->intent_p1;
      p_nheader1->intent_p2 = ph->intent_p2;
      p_nheader1->intent_p3 = ph->intent_p3;
    }
    void SetHeaderIntent(short ic, float p1, float p2, float p3) {
      p_nheader1->intent_code = ic;
      p_nheader1->intent_p1 = p1;
      p_nheader1->intent_p2 = p2;
      p_nheader1->intent_p3 = p3;
    }
    void SetHeaderIntent(short ic, float p1, float p2) {
      p_nheader1->intent_code = ic;
      p_nheader1->intent_p1 = p1;
      p_nheader1->intent_p2 = p2;
    }
    void SetHeaderIntent(short ic, float p1) {
      p_nheader1->intent_code = ic;
      p_nheader1->intent_p1 = p1;
    }
    void SetHeaderIntent(short ic) {
      p_nheader1->intent_code = ic;
    }
    void CopyHeaderUnits(NIFTIFile &fin) {
      nifti_1_header *ph;
      ph = fin.Header();
      p_nheader1->xyzt_units = ph->xyzt_units;
    }
    void CopyHeaderSliceInfo(NIFTIFile &fin) {
      nifti_1_header *ph;
      ph = fin.Header();
      p_nheader1->dim_info = ph->dim_info;
      p_nheader1->slice_duration = ph->slice_duration;
      p_nheader1->slice_code = ph->slice_code;
      p_nheader1->slice_start = ph->slice_start;
      p_nheader1->slice_end = ph->slice_end;
    
    }

    std::string Error() {return errorstring;};
    void SetOutputMode(NIFTIOutputMode nm) {nmode = nm;}

  private:
    // generic copy and recast function template
    template<class T1, class T2>
    void CopyBuffer(T1 *pdest, const T2 *psrc, size_t numel) {
      for (size_t ii=0; ii<numel; ++ii) {
	pdest[ii] = (T1)psrc[ii];
      }
    }
    // copy buffer and recast depending on nifti datatype
    template<class T>
    void MoveDataIn(T *pdataptr, const char *buffer, size_t numel, int datatype) {
      switch (datatype) {
      case NIFTI_TYPE_UINT8:
	CopyBuffer<T,uint8_t>(pdataptr,(uint8_t *)buffer,numel);
	break;
      case NIFTI_TYPE_INT8:
	CopyBuffer<T,int8_t>(pdataptr,(int8_t *)buffer,numel);
	break;
      case NIFTI_TYPE_UINT16:
	CopyBuffer<T,uint16_t>(pdataptr,(uint16_t *)buffer,numel);
	break;
      case NIFTI_TYPE_INT16:
	CopyBuffer<T,int16_t>(pdataptr,(int16_t *)buffer,numel);
	break;
      case NIFTI_TYPE_UINT32:
	CopyBuffer<T,uint32_t>(pdataptr,(uint32_t *)buffer,numel);
	break;
      case NIFTI_TYPE_INT32:
	CopyBuffer<T,int32_t>(pdataptr,(int32_t *)buffer,numel);
	break;
      case NIFTI_TYPE_UINT64:
	CopyBuffer<T,uint64_t>(pdataptr,(uint64_t *)buffer,numel);
	break;
      case NIFTI_TYPE_INT64:
	CopyBuffer<T,int64_t>(pdataptr,(int64_t *)buffer,numel);
	break;
      case NIFTI_TYPE_FLOAT32:
	CopyBuffer<T,float>(pdataptr,(float *)buffer,numel);
	break;
      case NIFTI_TYPE_FLOAT64:
	CopyBuffer<T,double>(pdataptr,(double *)buffer,numel);
	break;
      }
    }
    // copy buffer and recast depending on nifti datatype
    template<class T>
    void MoveDataOut(const T *pdataptr, char *buffer, size_t numel, int datatype) {
      switch (datatype) {
      case NIFTI_TYPE_UINT8:
	CopyBuffer<uint8_t,T>((uint8_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_INT8:
	CopyBuffer<int8_t,T>((int8_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_UINT16:
	CopyBuffer<uint16_t,T>((uint16_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_INT16:
	CopyBuffer<int16_t,T>((int16_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_UINT32:
	CopyBuffer<uint32_t,T>((uint32_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_INT32:
	CopyBuffer<int32_t,T>((int32_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_UINT64:
	CopyBuffer<uint64_t,T>((uint64_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_INT64:
	CopyBuffer<int64_t,T>((int64_t *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_FLOAT32:
	CopyBuffer<float,T>((float *)buffer,pdataptr,numel);
	break;
      case NIFTI_TYPE_FLOAT64:
	CopyBuffer<double,T>((double *)buffer,pdataptr,numel);
	break;
      }
    }

    void SetHeaderInfo(MRIBase &vol) {        
      vol.SetVoxelSize(p_nheader1->pixdim[1],p_nheader1->pixdim[2],
		       p_nheader1->pixdim[3]);
      OrientationMatrix orient;
      orient(0,0) = p_nheader1->srow_x[0];
      orient(0,1) = p_nheader1->srow_x[1];
      orient(0,2) = p_nheader1->srow_x[2];
      orient(0,3) = p_nheader1->srow_x[3];
      orient(1,0) = p_nheader1->srow_y[0];
      orient(1,1) = p_nheader1->srow_y[1];
      orient(1,2) = p_nheader1->srow_y[2];
      orient(1,3) = p_nheader1->srow_y[3];
      orient(2,0) = p_nheader1->srow_z[0];
      orient(2,1) = p_nheader1->srow_z[1];
      orient(2,2) = p_nheader1->srow_z[2];
      orient(2,3) = p_nheader1->srow_z[3];
      vol.SetOrient(orient);
    }
    template <class T>
    void SetHeaderInfo(MRIData<T> &vol) {        
      vol.SetVoxelSize(p_nheader1->pixdim[1],p_nheader1->pixdim[2],
		       p_nheader1->pixdim[3]);
      vol.SetTVoxelSize(p_nheader1->pixdim[4]);
      OrientationMatrix orient;
      orient(0,0) = p_nheader1->srow_x[0];
      orient(0,1) = p_nheader1->srow_x[1];
      orient(0,2) = p_nheader1->srow_x[2];
      orient(0,3) = p_nheader1->srow_x[3];
      orient(1,0) = p_nheader1->srow_y[0];
      orient(1,1) = p_nheader1->srow_y[1];
      orient(1,2) = p_nheader1->srow_y[2];
      orient(1,3) = p_nheader1->srow_y[3];
      orient(2,0) = p_nheader1->srow_z[0];
      orient(2,1) = p_nheader1->srow_z[1];
      orient(2,2) = p_nheader1->srow_z[2];
      orient(2,3) = p_nheader1->srow_z[3];
      vol.SetOrient(orient);
    }

    void GetHeaderInfo(const MRIBase &vol) {
      // InitHeader();
      /*
	p_nheader1->sizeof_hdr = 348;
	p_nheader1->vox_offset = 352;
	std::strcpy(p_nheader1->magic,"n+1");
	std::strcpy(p_nheader1->descrip,"niftifile.h");
      */
      p_nheader1->dim[0] = vol.NumDims();
      p_nheader1->dim[1] = vol.XSize();
      p_nheader1->dim[2] = vol.YSize();
      p_nheader1->dim[3] = vol.ZSize();
      p_nheader1->dim[4] = vol.TSize();
      p_nheader1->pixdim[1] = vol.XVoxelSize();
      p_nheader1->pixdim[2] = vol.YVoxelSize();
      p_nheader1->pixdim[3] = vol.ZVoxelSize();
      p_nheader1->pixdim[4] = vol.TVoxelSize();
      if (calc_orient) {
	p_nheader1->sform_code = 1;
	p_nheader1->srow_x[0] = vol.Orient(0,0);
	p_nheader1->srow_x[1] = vol.Orient(0,1);
	p_nheader1->srow_x[2] = vol.Orient(0,2);
	p_nheader1->srow_x[3] = vol.Orient(0,3);
	p_nheader1->srow_y[0] = vol.Orient(1,0);
	p_nheader1->srow_y[1] = vol.Orient(1,1);
	p_nheader1->srow_y[2] = vol.Orient(1,2);
	p_nheader1->srow_y[3] = vol.Orient(1,3);
	p_nheader1->srow_z[0] = vol.Orient(2,0);
	p_nheader1->srow_z[1] = vol.Orient(2,1);
	p_nheader1->srow_z[2] = vol.Orient(2,2);
	p_nheader1->srow_z[3] = vol.Orient(2,3);
      }  
    }

    void SetBitPix() {
      switch (p_nheader1->datatype) {
      case NIFTI_TYPE_UINT8:
	p_nheader1->bitpix = 8;
	break;
      case NIFTI_TYPE_INT8:
	p_nheader1->bitpix = 8;
	break;
      case NIFTI_TYPE_UINT16:
	p_nheader1->bitpix = 16;
	break;
      case NIFTI_TYPE_INT16:
	p_nheader1->bitpix = 16;
	break;
      case NIFTI_TYPE_UINT32:
	p_nheader1->bitpix = 32;
	break;
      case NIFTI_TYPE_INT32:
	p_nheader1->bitpix = 32;
	break;
      case NIFTI_TYPE_UINT64:
	p_nheader1->bitpix = 64;
	break;
      case NIFTI_TYPE_INT64:
	p_nheader1->bitpix = 64;
	break;
      case NIFTI_TYPE_FLOAT32:
	p_nheader1->bitpix = 32;
	break;
      case NIFTI_TYPE_FLOAT64:
	p_nheader1->bitpix = 64;
	break;
      }	
    }

    void GetOutputMode(const std::string &filename, NIFTIOutputMode &nm) {
      nm = NIFTIOutputMode::NOZIP;
      size_t slen = filename.size();
      if (slen < 4) {      
	return;
      }
      if (filename[slen-3] == '.') {
	if (filename[slen-2] == 'g' && filename[slen-1] == 'z') {
	  nm = NIFTIOutputMode::GZIP;
	}
      }

    }


    nifti_1_header *p_nheader1;
    // gzFile fgzio;
    FileHandleBase *pfio;
    NIFTIOutputMode nmode;
    std::string errorstring;
    bool calc_orient;
  };

}

#endif
